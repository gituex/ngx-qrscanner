/* eslint-disable */
var gulp = require('gulp'),
  gutil = require('gulp-util'),
  path = require('path'),
  merge = require('merge-stream'),
  ngc = require('@angular/compiler-cli/src/main').main,

  rollup = require('gulp-rollup'),
  nodeResolve = require('rollup-plugin-node-resolve'),
  commonjs = require('rollup-plugin-commonjs'),
  uglify = require('rollup-plugin-uglify'),
  minify = require('uglify-es').minify,

  rename = require('gulp-rename'),
  del = require('del'),
  sequence = require('gulp-sequence'),
  inlineResources = require('./tools/gulp/inline-resources'),
  _ = require('lodash');

const rootFolder = path.join(__dirname);
const srcFolder = path.join(rootFolder, 'src');
const tmpFolder = path.join(rootFolder, '.tmp');
const relativeTmpFolder = '.tmp';
const buildFolder = path.join(rootFolder, 'build');
const distFolder = path.join(rootFolder, 'dist');

const LIB_NAME = require('./package.json').name;
const FILE_NAME = 'ngx-qrscanner';

const rollupBaseConfig = {
  sourcemap: false,
  input: `${buildFolder}/index.js`,
  onwarn: function (warning) {
    if (warning.code === 'THIS_IS_UNDEFINED') {
      return;
    }
    console.warn(warning.message);
  },
  output: {
    globals: {
      '@angular/core': 'core',
      '@angular/common': 'common',
      '@angular/http': 'http',
      '@angular/router': 'router',
      '@angular/animations': 'animations',
      '@angular/forms': 'forms',
      '@angular/platform-browser': 'platformBrowser',
      'rxjs/operators': 'rxjsOperators'
    }
  },
  external: [
    '@angular/core',
    '@angular/common',
    '@angular/http',
    '@angular/router',
    '@angular/animations',
    '@angular/forms',
    '@angular/platform-browser',
    'rxjs/add/operator/filter',
    'rxjs/add/operator/timeout',
    'rxjs/operators'
  ],
  plugins: [
    nodeResolve({
      jsnext: true,
      module: true
    }),
    commonjs({
      include: 'node_modules/rxjs/**',
    })
  ]
}

gulp.task('clean:dist', function () {
  return deleteFolders([distFolder + '/**', '!' + distFolder]);
});

gulp.task('copy:source:tmp', function () {
  return gulp.src(`${srcFolder}/**/*`)
    .pipe(gulp.dest(tmpFolder));
});

gulp.task('inline-resources', function () {
  return Promise.resolve()
    .then(() => inlineResources(tmpFolder));
});

gulp.task('ngc', function () {
  ngc(['--project', `${tmpFolder}/tsconfig.json`]);
  return Promise.resolve();
});

gulp.task('ngc:move', function () {
  return gulp.src(`${buildFolder}/${relativeTmpFolder}/**/*.*`).pipe(gulp.dest(buildFolder));
});

gulp.task('ngc:remove', function () {
  return deleteFolders(`${buildFolder}/${relativeTmpFolder}`);
});

gulp.task('rollup:es', function () {
  return gulp.src(`${buildFolder}/**/*.js`)
    .pipe(rollup(_.merge({}, rollupBaseConfig, {
      output: {
        format: 'es'
      },
      plugins: rollupBaseConfig.plugins.concat([uglify({}, minify)])
    })))
    .pipe(rename(`${FILE_NAME}.es5.js`))
    .pipe(gulp.dest(distFolder));
});

gulp.task('rollup:umd', function () {
  return gulp.src(`${buildFolder}/**/*.js`)
    .pipe(rollup(_.merge({}, rollupBaseConfig, {
      output: {
        name: LIB_NAME,
        format: 'umd'
      }
    })))
    .pipe(rename(`${FILE_NAME}.js`))
    .pipe(gulp.dest(distFolder));
});

gulp.task('copy:build:dist', function () {
  return gulp.src([
      `${buildFolder}/**/*`,
      `!${buildFolder}/node_modules/**/*`,
      `!${buildFolder}/node_modules`,
      `!${buildFolder}/**/*.js`
    ])
    .pipe(gulp.dest(distFolder));

  return
});

gulp.task('copy:styles:dist', function () {
  var folder = gulp.src(`${srcFolder}/modules/_styles/**/*`)
    .pipe(gulp.dest(`${distFolder}/modules/_styles`));

  var variables_file = gulp.src(`${srcFolder}/modules/_variables.scss`)
    .pipe(gulp.dest(`${distFolder}/modules`));

  var base_file = gulp.src([
    `${srcFolder}/styles.scss`,
    `${srcFolder}/color-definitions.scss`
  ])
    .pipe(gulp.dest(distFolder));

  return merge(folder, variables_file, base_file);
});

gulp.task('copy:assets:dist', function () {
  return gulp.src([`${srcFolder}/assets/**/*`])
    .pipe(gulp.dest(`${distFolder}/assets`));
});

gulp.task('copy:manifest:dist', function () {
  return gulp.src([`${srcFolder}/package.json`])
    .pipe(gulp.dest(distFolder));
});

gulp.task('copy:readme:dist', function () {
  return gulp.src([path.join(rootFolder, 'README.MD')])
    .pipe(gulp.dest(distFolder));
});

gulp.task('clean:tmp', function () {
  return deleteFolders([tmpFolder]);
});

gulp.task('clean:build', function () {
  return deleteFolders([buildFolder]);
});

gulp.task('clean', sequence('clean:dist', 'clean:tmp', 'clean:build'));

gulp.task('compile', sequence(
    'clean',
    'copy:source:tmp',
    'inline-resources',
    'ngc',
    'ngc:move',
    'ngc:remove',
    'rollup:es',
    'rollup:umd',
    'copy:build:dist',
    'copy:styles:dist',
    'copy:assets:dist',
    'copy:manifest:dist',
    'copy:readme:dist',
    'clean:build',
    'clean:tmp'
  ));

gulp.task('compile:dirt', sequence(
    'copy:source:tmp',
    'inline-resources',
    'ngc',
    'ngc:move',
    'ngc:remove',
    'rollup:es',
    'rollup:umd',
    'copy:build:dist',
    'copy:styles:dist',
    'copy:assets:dist',
    'copy:manifest:dist',
    'copy:readme:dist',
    'clean:build',
    'clean:tmp'
  ));

gulp.task('watch', function () {
  gulp.watch(`${srcFolder}/**/*`, ['compile:dirt']);
});

gulp.task('build', sequence('compile'));
gulp.task('build:watch', sequence('build', 'watch'));
gulp.task('default', sequence('build:watch'));

function deleteFolders(folders) {
  return del(folders);
}
